# vue-data-state Lightweight state management

## Description
Similar to vuex's lightweight data, state, cache integrated management method.

* Global state，use it like Vuex. 
* Local state，It is valid within the parent component and its child components, and does not affect the sibling component and superior component.
* Initialization, the global state can be initialized assignment.

It only contains the state, but does not contain the operation methods 、mutations and action. You can use Vue3 Composition API to operate the state.
 

## Source code

https://gitee.com/naturefw/vue-data-state  

<script src='https://gitee.com/naturefw/vue-data-state/widget_preview' async defer></script>
<div id="osc-gitee-widget-tag"></div>
<style>
.osc_pro_color {color: #ffffff !important;}
.osc_panel_color {background-color: #1e252b !important;}
.osc_background_color {background-color: #323d47 !important;}
.osc_border_color {border-color: #455059 !important;}
.osc_desc_color {color: #d7deea !important;}
.osc_link_color * {color: #99a0ae !important;}
</style>


## Installation

1.  npm i vue-data-state


#### Instructions

like Vuex.

1.  VuexDataState.createStore Create instance, define global state, local state and initialization function.
2.  main.js to app
3.  template use $state 
4.  code use VueDS.useStore() 

