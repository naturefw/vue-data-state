// 模仿Vuex写一个简单的数据、状态、缓存管理

import { reactive, readonly } from 'vue'
import trackReactive from './trackReactive.js'
/**
 * 做一个简单的状态
 */
export default {
  /**
   * 状态的容器，reactive 的形式
   */
  state: {},
  /**
   * 全局状态的跟踪日志
   */
  changeLog: [],

  /**
   * 内部钩子，key：数组
   */
  _watch: {},

  /**
   * 外部函数，设置钩子，key：回调函数
   */
  watch: {},

  /**
   * 状态的初始化回调函数，async
   */
  init: () => {},

  /**
   * 创建全局状态
   * @param {object} info 状态参数
   * * info = { // 状态名称不能重复
   * * * // 全局状态，不支持跟踪、钩子、日志
   * * * state: {
   * * * *  user1: { // 每个状态都必须是对象，不支持基础类型
   * * * * *  name: 'jyk' //
   * * * * }
   * * * },
   * * * // 只读状态，不支持跟踪、钩子、日志，只能用初始化回调函数的参数修改
   * * * readonly: {
   * * * *  user2: { // 每个常量都必须是对象，不支持基础类型
   * * * * *  name: 'jyk' //
   * * * * }
   * * * },
   * * * // 可跟踪状态，支持跟踪、钩子、日志
   * * * track: {
   * * * *  user3: { // 每个状态都必须是对象，不支持基础类型
   * * * * *  name: 'jyk' //
   * * * * }
   * * * },
   * * * // 初始化函数，可以从后端、前端等获取数据设置状态
   * * * // 设置好状态的容器后调用，可以获得只读状态的可写参数
   * * * init(state, _readonly) {}
   * * }
   * @returns vue需要的插件
   */
  createStore (info) {
    // 把 state 存入 state
    for (const key in info.state) {
      const s = info.state[key]
      // 外部设置空钩子
      this.watch[key] = (e) => {}
      this.state[key] = reactive(s)
    }
    // 把 readonly 存入 state
    const _readonly = {}
    for (const key in info.readonly) {
      const s = info.readonly[key]
      _readonly[key] = reactive(s) // 设置一个可以修改状态的 reactive
      this.state[key] = readonly(_readonly[key]) // 对外返回一个只读的状态
    }
    // 把 track 存入 state
    for (const key in info.track) {
      const s = reactive(info.track[key])
      // 指定的状态，添加监听的钩子，数组形式
      this._watch[key] = []
      // 外部设置钩子
      this.watch[key] = (e) => {
        // 把钩子加进去
        this._watch[key].push(e)
      }
      this.state[key] = trackReactive(s, key, this.changeLog, this._watch[key])
    }
   
    // 调用初始化函数
    if (typeof info.init === 'function') {
      info.init(this.state, _readonly)
    }

    const _store = this
    return {
      // 安装插件
      install (app, options) {
        // 设置模板直接使用状态
        app.config.globalProperties.$state = _store.state
        // 发现个问题，这是个object，不用注入到 provide
        // app.provide(_storeFlag, _store)
        // 调用初始化，给全局状态赋值
        // _store.init(_store.state, _readonly)
      }
    }
  }
}
