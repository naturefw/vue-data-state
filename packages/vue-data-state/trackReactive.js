
import { isReactive, toRaw } from 'vue'

// 修改深层属性时，记录属性路径
let _getPath = []

/**
 * 带跟踪的reactive。使用 proxy 套娃
 * @param {reactive} _target  要拦截的目标 reactive
 * @param {string} flag 状态名称
 * @param {array} log 存放跟踪日志的数组
 * @param {array} watch 监听函数
 * @param {array} base 根对象
 * @param {array} _path 嵌套属性的各级属性名称的路径
 */
export default function trackReactive (_target, flag, log = [], watch = null, base = null, _path = []) {
  // 记录根对象
  const _base = toRaw(_target)
  // 修改嵌套属性的时候，记录属性的路径
  const getPath = () => {
    if (!base) return []
    else return _path
  }
  
  const proxy = new Proxy(_target, {
    // get 不记录日志，没有钩子，不拦截
    get: function (target, key, receiver) {
      const __path = getPath(key)
      _getPath = __path
      // 调用原型方法
      const res = Reflect.get(target, key, receiver)
      // 记录
      if (typeof key !== 'symbol') {
        // console.log(`getting ${key}!`, target[key])
        switch (key) {
          case '__v_isRef':
          case '__v_isReactive':
          case '__v_isReadonly':
          case '__v_raw':
          case 'toString':
          case 'toJSON':
            // 不记录
            break
          default:
            // 嵌套属性的话，记录属性名的路径
            __path.push(key) 
            break
        }
      }
      if (isReactive(res)) {
        // 嵌套的属性
        return trackReactive(res, flag, log, watch, _base, __path)
      }
      return res
    },
    set: function (target, key, value, receiver) {
      const stack = new Error().stack
      const arr = stack.split('\n')
      const stackstr = arr.length > 1 ? arr[2]: '' // 记录调用的函数

      const _log = {
        stateKey: flag, // 状态名
        keyPath: base === null ? '' : _getPath.join(','), //属性路径
        key: key, // 要修改的属性
        value: value, // 新值
        oldValue: target[key], // 原值
        stack: stackstr, // 修改状态的函数和组件
        time: new Date().valueOf(), // 修改时间
        // targetBase: base, // 根
        target: target // 上级属性/对象
      }
      // 记录日志
      log.push(_log)
      if (log.length > 100) {
        log.splice(0, 30) // 去掉前30个，避免数组过大
      }

      // 设置钩子，依据回调函数决定是否修改
      let reValue = null
      if (typeof watch === 'function') {
        const re = watch(_log) // 执行钩子函数，获取返回值
        if (typeof re !== 'undefined')
          reValue = re
      } else if (typeof watch.length !== 'undefined') {
        watch.forEach(fun => { // 支持多个钩子
          const re = fun(_log) // 执行钩子函数，获取返回值
          if (typeof re !== 'undefined')
            reValue = re
        })
      } 

      // 记录钩子返回的值
      _log.callbackValue = reValue
      // null：可以修改，使用 value；其他：强制修改，使用钩子返回值
      const _value = (reValue === null) ? value : reValue
      _log._value = _value
      
      // 调用原型方法
      const res = Reflect.set(target, key, _value, target)
      return res
    }
  })
  // 返回实例
  return proxy
}
