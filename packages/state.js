// 引入各种函数，便于做成npm包
// indexedDB 部分
import nfStore from './vue-data-state/index.js'
import trackReactive from './vue-data-state/trackReactive.js'

/**
 * 状态的容器，reactive 的形式
 */
const state = nfStore.state

/**
 * 全局状态的跟踪日志
 */
const changeLog = nfStore.changeLog

/**
 * 设置set的钩子
 */
const watchState = nfStore.watch

/**
 * 创建全局状态
  * @param {object} info 状态参数
  * * info = { // 状态名称不能重复
  * * * // 全局状态，不支持跟踪、钩子、日志
  * * * state: {
  * * * *  user1: { // 每个状态都必须是对象，不支持基础类型
  * * * * *  name: 'jyk' //
  * * * * }
  * * * },
  * * * // 只读状态，不支持跟踪、钩子、日志，只能用初始化回调函数的参数修改
  * * * readonly: {
  * * * *  user2: { // 每个常量都必须是对象，不支持基础类型
  * * * * *  name: 'jyk' //
  * * * * }
  * * * },
  * * * // 可跟踪状态，支持跟踪、钩子、日志
  * * * track: {
  * * * *  user3: { // 每个状态都必须是对象，不支持基础类型
  * * * * *  name: 'jyk' //
  * * * * }
  * * * },
  * * * // 初始化函数，可以从后端、前端等获取数据设置状态
  * * * // 设置好状态的容器后调用，可以获得只读状态的可写参数
  * * * init(state, _readonly) {}
 * * }
 * @returns vue需要的插件
 */
const createStore = (info) => nfStore.createStore(info)

export {
  state,
  watchState,
  changeLog,
  createStore,
  trackReactive
}
