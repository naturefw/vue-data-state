import VuexDataState from '../../packages/vue-data-state/'

export default VuexDataState.createStore({
  global: { // 全局状态，global 名称不能改。
    blogState: {
      name: '全局状态演示', //
      currentGroupId: 0 // 选择的分组ID。0：没选择
    },
    dicussState: {
      name: '另一个状态'
    }
  },
  local: { // 局部状态，local 名称不能改。
    localState() { // 函数形式需要 return 对象，名称随意，可以写多个
      return {
        name: '局部状态', //
        page: { // 分页参数
          pageIndex: 1
        }
      }
    }
  },
  // 可以给全局状态设置初始状态，同步数据可以直接在上面设置，如果是异步数据，可以在这里设置。
  init(state) {

    setTimeout(() => {
      state.blogState.name = 'init 设置初始状态，可以异步'
    },3000)
  }
}) 