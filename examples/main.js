import { createApp } from 'vue'
import App from './App.vue'
import store from './store-ds' // 轻量级状态

createApp(App)
  .use(store)
  .mount('#app')
